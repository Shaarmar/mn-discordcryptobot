FROM python:3.7 as dependencies

COPY requirements.txt /

RUN  pip install -r requirements.txt

ENV DISCORD_API=${DISCORD_API}
ENV CRYPTO_COMPARE_API=${CRYPTO_COMPARE_API}

FROM dependencies as application
COPY . /
CMD ["sh", "start_cryptobot.sh"]
