# Discord Crypto Bot

Bot for scheduled news and market updates as well as commands for retrieving price info of coins.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* Python 3
* Pip
* Virtualenv

# Quickstart
To run bot locally:

    > git clone https://gitlab.com/Shaarmar/mn-discordcryptobot.git
    > cd bot
    > virtualenv venv -p python3
    > source venv/bin/activate
    > pip install -r requirements.txt
    > python discordBot.py

## One-Click Deploy Button

### Pre-reqs for deploying this project to Heroku:
 * Create a Discord Dev Account [Discord Account](https://discordapp.com/register?redirect_to=%2Fdevelopers%2Fapplications%2F)
 * Create a free [Heroku account](https://signup.heroku.com/) 
 * Create an [Crypto Compare Account](https://min-api.cryptocompare.com)
 * Install the [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli)

[![Deploy](https://www.herokucdn.com/deploy/button.png)](https://heroku.com/deploy)

## What if you just want to use my bot? 

Go ahead and add it to your server :)

[Invite Link](https://discordapp.com/api/oauth2/authorize?client_id=416290595259285515&permissions=0&scope=bot)
## Contributing

Please read [CONTRIBUTING.md](https://gitlab.com/Shaarmar/DiscordCryptoBot/blob/master/CONTRIBUTING.md) for details on code of conduct, and the process for submitting pull requests.

## Versioning

I use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://gitlab.com/Shaarmar/DiscordCryptoBot/tags). 

## Authors

* **Omar Shaarawi** 

See also the list of [contributors](https://gitlab.com/Shaarmar/DiscordCryptoBot/graphs/master) who participated in this project.
