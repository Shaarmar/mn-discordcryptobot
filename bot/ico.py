import requests
import json
import re
import httplib2
from bs4 import BeautifulSoup
from fuzzywuzzy import process
from datetime import datetime
import Levenshtein
import feedparser



present = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

http = httplib2.Http()


link = "https://api.trackico.io/v1/icos/"
r = requests.get(link).json
#print('this is r',r)
jdata= r
#print(jdata)

def coinDeskHeadlines():
    d = feedparser.parse('http://feeds.feedburner.com/CoinDesk')
    response = '__**Here are the Top 5 Crypto Headlines from CoinDesk:**__\n\n'
    for x in range(1,6):
        try:
            title = d['items'][x]['title']
            link = d['items'][x]['links'][0]['href']

            response += '**%s**:\n<%s>\n\n' % (title, link)

        except:
            print("Uhhh having trouble connecting. Try again in a bit.")
    return(response)


print(coinDeskHeadlines())

def getAllTitles():
    dicts = {}
    list1 = []
    for i in jdata:
        #print(i['slug'],i['title'])
        slug = i['slug']
        url = 'https://www.trackico.io/ico/{0}/'
        url = url.format(slug)
        r1 = requests.get(url)
        html_content = r1.text
        # print(html_content)
        soup = BeautifulSoup(html_content, "html.parser")
        #title = soup.find('title')
        #x = title.string
        #print(x[x.find("(") + 1:x.find(")")],i['title'])
        #dicts[x[x.find("(") + 1:x.find(")")]] = i['title']
        list1.append(i['slug'])
    return list1

#print(getAllTitles())
# with open('ico.json', 'w') as fp:
#     json.dump(getAllTitles(), fp)
#data2 = json.load(open('ico.json'))


def findCoin(coin):
    data = json.load(open('write.json'))
    data2 = json.load(open('ico.json'))

    coin1 = data.get(coin) # write json
    coin2 = data2.get(coin) # ico json?

    y1 = process.extractOne(coin, data2.values())

    if coin1 != None:
        for crypto in jdata:
            crypto_name = crypto["title"]
            if re.search(coin1, crypto_name, re.IGNORECASE):
                if len(coin1) == len(crypto_name):
                    return crypto["title"],crypto['url'],crypto['ico_start'],crypto['slug'],crypto['pre_ico_start'],crypto['pre_ico_end'],crypto['ico_end'],crypto['image']
    elif coin2 != None:
        for crypto in jdata:
            crypto_name = crypto["title"]
            if re.search(coin2, crypto_name, re.IGNORECASE):
                if len(coin2) == len(crypto_name):
                    return crypto["title"],crypto['url'],crypto['ico_start'],crypto['slug'],crypto['pre_ico_start'],crypto['pre_ico_end'],crypto['ico_end'],crypto['image']
    else:
        if (y1[1] > 80):
            coin = y1[0]
            if coin != None:
                for crypto in jdata:
                    crypto_name = crypto["title"]
                    if re.search(coin, crypto_name, re.IGNORECASE):
                        if len(coin) == len(crypto_name):
                            return crypto["title"],crypto['url'],crypto['ico_start'],crypto['slug'],crypto['pre_ico_start'],crypto['pre_ico_end'],crypto['ico_end'],crypto['image']


    return "Couldn't find project"



def getLinks(coin):
    link_list = []
    dict = {}
    try:
        title,url,ico_start,slug,pre_ico_start,pre_ico_end,ico_end,image= findCoin(coin)
    except ValueError:
        return "Could't find project"

    url = 'https://www.trackico.io/ico/{0}/#tab-financial'
    url = url.format(slug)
    r1 = requests.get(url)
    html_content = r1.text
    soup = BeautifulSoup(html_content, "html.parser")
    for link in soup.findAll('a', attrs={'onclick': re.compile("button-homepage|button-whitepaper|link-telegram|link-twitter|link-slack|link-facebook|link-github|link-reddit|link-discord")}):
        link_list.append(link.get('href'))
    dict["Website"] = link_list[0]
    for i  in link_list:
        if i.endswith(".pdf"):
            dict["Whitepaper"] = i
        if i.startswith('https://t.me/') or i.startswith('https://www.t.me/'):
            dict["Telegram"] = i
        if i.startswith('https://twitter.com/') or i.startswith('https://www.twitter.com/'):
            dict["Twitter"] = i
        if i.startswith('https://slack.')or i.startswith('https://www.slack.'):
            dict["Slack"] = i
        if i.startswith('https://facebook.com/') or i.startswith('https://www.facebook.com/') or i.startswith('http://www.facebook.com/') or i.startswith('http://www.facebook.com/') :
            dict["Facebook"] = i
        if i.startswith('https://github') or i.startswith('https://www.github'):
            dict["Github"] = i
        if i.startswith('https://reddit') or i.startswith('https://www.reddit'):
            dict["Reditt"] = i
        if i.startswith('https://discord') or i.startswith('https://www.discord'):
            dict["Discord"] = i

    return dict

def getInfo2(coin):
    try:
        title,url,ico_start,slug,pre_ico_start,pre_ico_end,ico_end,image= findCoin(coin)
    except ValueError:
        raise Exception
    url = 'https://www.trackico.io/ico/{0}/#tab-financial'
    url = url.format(slug)
    r1 = requests.get(url)
    html_content = r1.text
    soup = BeautifulSoup(html_content, "html.parser")
    t = soup.find('title')

    x = t.string
    symbol = x[x.find("(") + 1:x.find(")")]

    for i in soup.findAll('div', attrs={'class': re.compile("fs-14")}):
        desc = (i.find('p').get_text())


    currPrice = soup.find("th", text="Current price")
    if currPrice is None:
        currPrice = "NA"
    else:
        currPrice = soup.find("th", text="Current price").find_next_sibling("td").text
        currPrice = currPrice.splitlines()[0]


    ROI = soup.find("th", text="Return on investment (ROI)")
    if ROI is None:
        ROI = 'NA'
    else:
        ROI = soup.find("th", text="Return on investment (ROI)").find_next_sibling("td").text

    tfVol = soup.find("th", text="Volume (24h)")
    if tfVol is None:
        tfVol = "NA"
    else:
        tfVol = soup.find("th", text="Volume (24h)").find_next_sibling("td").text

    circSup = soup.find("th", text="Available supply")
    if circSup is None:
        circSup = 'NA'
    else:
        circSup = soup.find("th", text="Available supply").find_next_sibling("td").text

    totSup = soup.find("th", text="Total supply")
    if totSup is None:
        totSup = "NA"
    else:
        totSup = soup.find("th", text="Total supply").find_next_sibling("td").text

    marketCap = soup.find("th", text="Market cap")
    if marketCap is None:
        marketCap = 'NA'
    else:
        marketCap = soup.find("th", text="Market cap").find_next_sibling("td").text


    tokenForSale = soup.find("th", text="Token for sale")
    if tokenForSale is None:
        tokenForSale = "NA"
    else:
        tokenForSale = soup.find("th", text="Token for sale").find_next_sibling("td").text
        tokenForSale = tokenForSale.replace('\n', '')


    tokenSupply = soup.find("th", text="Token supply")
    if tokenSupply is None:
        tokenSupply = "NA"
    else:
        tokenSupply = soup.find("th", text="Token supply").find_next_sibling("td").text

    hardCap = soup.find("th", text="Hard cap")
    if hardCap is None:
        hardCap = "NA"
    else:
        hardCap = soup.find("th", text="Hard cap").find_next_sibling("td").text

        if hardCap.splitlines()[0] == '':
            hardCap = hardCap.splitlines()[1]
        else:
            hardCap = hardCap.splitlines()[0]

    KYC = soup.find("th", text="Know Your Customer")
    if KYC is None:
        KYC = "NA"
    else:
        KYC = soup.find("th", text="Know Your Customer").find_next_sibling("td").text
        KYC = KYC.replace("\n", "")

    Whitelist = soup.find("th", text="Whitelist")
    if Whitelist is None:
        Whitelist = "NA"

    else:
        Whitelist = soup.find("th", text="Whitelist").find_next_sibling("td").text
        Whitelist = Whitelist.replace("\n", "")



    return title,url,desc,image,pre_ico_start, pre_ico_end, ico_start, ico_end, tokenForSale, tokenSupply, Whitelist, KYC, hardCap,currPrice,ROI,tfVol,circSup,totSup,marketCap,symbol




