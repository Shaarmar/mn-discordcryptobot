import json
import asyncio
import re
import discord
import requests

from discord.ext import commands

from datetime import datetime,date, time, timedelta
from dateutil.parser import parse
import subprocess
import os
import bot_functions as bf
import chart as c
import sentry_sdk
from sentry_sdk import capture_exception, configure_scope

sentry_sdk.init("https://9eb5dd6c01b34a7a9f293f2780cd32b6@sentry.io/1437291")

present = datetime.utcnow().strftime("%Y-%m-%d %H:%M:%SZ")

DISCORD_API_KEY= os.environ['DISCORD_API']

sixAM = '06:00'
nineAM = '09:00'
noon = '12:00'
threePM = '15:00'
sixPM = '18:00'
ninePM = '21:00'
elevenAM = '11:00'



class MyClient(commands.Bot):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs,command_prefix="!")

        # create the background task and run it in the background
        self.bg_task1 = self.loop.create_task(self.global_info())
        self.bg_task5 = self.loop.create_task(self.coin_desk_headlines())



    async def on_ready(self):
        print('Logged in as')
        print(self.user.name)
        print(self.user.id)
        print('------')



    async def coin_desk_headlines(self):
        await self.wait_until_ready()
        channel = self.get_channel(506887487461916689) # news channel  506887487461916689
        used = []
        while not self.is_closed():
            now = datetime.strftime(datetime.now(),'%H:%M')
            if now == noon or now == threePM or now == sixPM:
                title,img_url,description,link,published_on=bf.news(0)
                if title in used:
                    title,img_url,description,link,published_on=bf.news(1)
                    if title in used:
                        title,img_url,description,link,published_on=bf.news(2)
                        if title in used:
                            title,img_url,description,link,published_on=bf.news(3)
                            if title in used:
                                title,img_url,description,link,published_on=bf.news(4)
                                if title in used:
                                    title,img_url,description,link,published_on=bf.news(5)

                embed=discord.Embed(title=title,url=link,description=description)
                embed.set_image(url=img_url)
                embed.set_footer(text="Published on: " + published_on)

                used.append(title)

                await channel.send(embed=embed)
                t = 10800
            else:
                t = 1
            await asyncio.sleep(t)


    async def global_info(self):
        await self.wait_until_ready()
        channel = self.get_channel(506887527681228811) #  506887527681228811
        while not self.is_closed():
            now = datetime.strftime(datetime.now(),'%H:%M')
            if now == elevenAM:
                cryptocurrencies_number,market_cap_ath_date,market_cap_ath_value,market_cap_change_24h,market_cap_usd,volume_24h_change_24h,volume_24h_usd,bitcoin_dominance_percentage,last_updated = bf.global_info()
                c.market_cap_plot()

                embed= discord.Embed(title="Global Info",url="https://coinpaprika.com/overview/", description="Market cap all time high: " + market_cap_ath_value + " on " + market_cap_ath_date)
                embed.add_field(name="BTC Dominance", value=bitcoin_dominance_percentage, inline=True)
                embed.add_field(name="Market Cap", value=market_cap_usd + ' ('+market_cap_change_24h+')', inline=True)
                embed.add_field(name="Volume" , value=volume_24h_usd + ' ('+volume_24h_change_24h + ')', inline=True)
                embed.add_field(name="Number of Cryptos", value=cryptocurrencies_number, inline=False)
                f = discord.File("/images/market.png", filename="market.png")
                embed.set_image(url = "attachment://market.png")
                embed.set_footer(text="Last updated: " + last_updated)

                await channel.send(file= f,embed=embed)
                t = 86400
            else:
                t = 1
            await asyncio.sleep(t)



client = MyClient()

@client.event
async def on_command_error(ctx, error):
    if isinstance(error, (commands.MissingRequiredArgument, commands.BadArgument)):
        await ctx.send("`{}: {}`".format(type(error).__name__, error))
        capture_exception(error)
        formatted_help = await client.formatter.format_help_for(ctx, ctx.command)
        with configure_scope() as scope:
            scope.set_extra('channel.name', ctx.channel.name)
            scope.set_extra('author.name', ctx.author.name)
            scope.set_extra('author.nick', ctx.author.nick)
            scope.set_extra('author.discriminator', ctx.author.discriminator)
            scope.set_extra('author.display_name', ctx.author.display_name)
            scope.set_extra('message.id', ctx.message.id)
            scope.set_extra('message.content', ctx.message.content)
            scope.set_extra('message.jump_url', ctx.message.jump_url)
            scope.set_extra('prefix', ctx.prefix)
            scope.set_extra('invoked_with', ctx.invoked_with)

        for page in formatted_help:
            await ctx.send(page)

@client.command()
async def price(ctx,coin):
    coin  = coin.upper()
    try:
        cur_price, oneHour, tfHour, svnDay, name, Vol, totalSup, rank, circSup, marketCap,emoji,emoji1,emoji2,symbol,ath_price,ath_date,last_updated,logo_url,chart_url,link_url,btc_price= bf.ticker_info(coin.upper())
        c.get_plot(coin)
    except Exception as e:
        capture_exception(e)
        await ctx.send("`Coin "+coin+" could not be found`")




    embed = discord.Embed(title="Coin info: "+ name + " ("+symbol+")", description="1h: " + emoji + oneHour + " | 24h: " + emoji1 + tfHour+ " | 7d: "+ emoji2+svnDay, url = link_url,color=0x7a0019)
    embed.add_field(name="Current Price", value=cur_price,inline=True)
    embed.add_field(name="Satoshi Price", value=btc_price,inline=True)

    embed.add_field(name="Total Supply", value=str(totalSup) +' '+symbol, inline=True)
    embed.add_field(name="Circulating Supply", value=str(circSup)+" "+symbol,inline=True)
    embed.add_field(name="Market Cap"+" ("+str(rank)+")", value=marketCap, inline=True)
    embed.add_field(name="Volume", value=Vol, inline=True)
    f = discord.File("/images/price.png", filename="price.png")

    embed.set_image(url = "attachment://price.png")
    embed.set_thumbnail(url = logo_url )
    embed.set_footer(text="Last updated: " + last_updated)
    await ctx.send(file= f,embed=embed)

@client.command()
async def priceH(ctx,coin,date):

    try:
        date = datetime.strptime(date, "%m/%d/%Y").strftime('%Y-%m-%d')
    except ValueError as e:
        capture_exception(e)
        await ctx.send("`Incorrect date format, should be Month/Day/Year`")

    try:
        market_cap,price,timestamp,volume_24h,logo_url = bf.ticker_historical_info(coin.upper(),date)
    except ValueError as e:
        capture_exception(e)
        await ctx.send("`Coin " + coin +" not found`")


    embed=discord.Embed(title="Historical Ticker", description="Date: "+ timestamp)
    embed.set_thumbnail(url = logo_url )
    embed.add_field(name="Price", value=price, inline=True)
    embed.add_field(name="Market Cap", value=market_cap, inline=True)
    embed.add_field(name="Volume 24h", value=volume_24h, inline=True)

    await ctx.send(embed=embed)

@client.command()
async def exchange(ctx,exchange_name):
    try:
        active,adjusted_rank,api_status,currencies,description,fiats,last_updated,website,markets,name,website_status,img_url = bf.exchange_info1(exchange_name)
    except ValueError as e:
        capture_exception(e)
        await ctx.send("`"+exchange_name+" could not be found`")

    embed=discord.Embed(title="Active Status: " + active + "| Api Status: " + api_status+ "| Website Status: "+ website_status, description=description)
    embed.set_author(name=name, url=website)
    embed.set_thumbnail(url=img_url)
    embed.add_field(name="Rank", value=adjusted_rank, inline=True)
    embed.add_field(name="# of Currencies", value=currencies, inline=True)
    embed.add_field(name="Fiats", value=fiats, inline=True)
    embed.add_field(name="# of Markets", value=markets, inline=True)
    embed.set_footer(text="Last Updated: " + last_updated)

    await ctx.send(embed=embed)

@client.command()
async def toCrypto(ctx,amount,base):
    try:
        price = "`$" +amount+" of "+ base + " is " +bf.price_converter_to_crypto(amount,base)+"`"
    except ValueError as e:
        capture_exception(e)
        ctx.send("`Coin "+base+" could not be found`")

    await ctx.send(price)

@client.command()
async def toUSD(ctx,amount,base):
    try:
        price = "`" + amount+" "+ base + " is " + bf.price_converter_to_USD(amount,base)+ "`"
    except ValueError as e:
        capture_exception(e)
        await ctx.send("`Coin "+base+" could not be found`")

    await ctx.send(price)

client.run(DISCORD_API_KEY)
