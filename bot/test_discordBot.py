import pytest
import bot_functions as bf

@pytest.fixture
def my_client():
    return bf.Client()

@pytest.mark.parametrize("endpoint", [
    ("coins"),
    ("coins/btc-bitcoin"),
    ("coins/btc-bitcoin/twitter"),
    ("coins/btc-bitcoin/events"),
    ("coins/btc-bitcoin/exchanges"),
    ("coins/btc-bitcoin/markets"),
    ("coins/btc-bitcoin/ohlcv/latest"),
    ("coins/btc-bitcoin/ohlcv/historical?start=2019-01-01&end=2019-01-20"),
    ("coins/btc-bitcoin/ohlcv/today"),
    ("people/vitalik-buterin"),
    ("tags"),
    ("tickers/btc-bitcoin"),
    ("tickers/btc-bitcoin/historical?start=2019-01-01&end=2019-01-20"),
    ("exchanges"),
    ("exchanges/binance"),
    ("exchanges/binance/markets"),
    ("search?q=btc"),
    ("price-converter?base_currency_id=btc-bitcoin&quote_currency_id=usd-us-dollars"),
])
def test_end_point(endpoint):
    client = my_client()
    assert client.get_status(endpoint) == 200


