import pandas as pd
import simplejson as json

from dateutil.parser import parse
import plotly.offline as pyo
import plotly.graph_objs as go
import cryptocompare
import plotly.io as pio
import bot_functions as bf

import plotly.plotly as py
import plotly as p
import requests
import time

p.tools.set_credentials_file(username='Shaarmar', api_key='iaZUPf63CkrEU2D7Rs7K')



def get_response(url):
    dictList = []
    data = requests.get(url).json()["market_cap_by_available_supply"]
    df = pd.DataFrame(data)
    return df



def market_cap_plot():
    current_milli_time = lambda: int(round(time.time() * 1000))
    now = current_milli_time()
    seven_day_milli_time = now - 604800000
    url = "https://graphs2.coinmarketcap.com/global/marketcap-total/{0}/{1}".format(seven_day_milli_time,now)
    df = get_response(url)
    df.rename( columns={0 :'time', 1:'price'}, inplace=True )
    df["time"] = pd.to_datetime(df.time, unit='ms')

    # Create a trace
    trace = go.Scatter(
        x = df["time"],
        y = df["price"],
        line = dict(
        color = ('rgb(22, 96, 167)'),
        width = 5,
       )
    )

    data = [trace]


    layout = go.Layout(
        xaxis=dict(


            tickfont=dict(
            family='Old Standard TT, serif',
            size=30,
            color='white'
            ),
            ),
        plot_bgcolor='rgba(0,0,0,0)',
        paper_bgcolor='rgba(0,0,0,0)',

        width=5000,
        height=5000,


        margin=go.layout.Margin(
            l=100,
            r=0,
            b=100,
            t=50,
            pad=1
        ),
        yaxis=dict(
             tickfont=dict(
            family='Old Standard TT, serif',
            size=30,
            color='white'
        ),
        )
    )

    fig = dict(data=data, layout=layout)

    py.image.save_as({'data': fig},'/images/market.png',width=1200, height=500)




def get_plot(symbol):

    try:
        x = cryptocompare.get_historical_price_hour(str(symbol).upper(), curr='USD')
        price = bf.ticker_historical_price(symbol.upper())
    except Exception as e:
        raise TypeError("Symbol isn't correct or it's not on cryptocompare's api")

    #print(x)
    #print(price)


    s = json.dumps(x,indent = 2, sort_keys = True)
    states = json.loads(s)

    #print(states)

    df = pd.DataFrame(states['Data'])
    df["time"] = pd.to_datetime(df.time, unit='s')

    layout = go.Layout(
        xaxis=dict(
            autorange=True,
            showgrid=False,
            zeroline=False,
            showline=False,
            ticks='',
            showticklabels=False,

        ),
        plot_bgcolor='rgba(0,0,0,0)',
        paper_bgcolor='rgba(0,0,0,0)',
        showlegend=False,
        autosize=False,
        width=5000,
        height=5000,


        margin=go.layout.Margin(
            l=0,
            r=0,
            b=0,
            t=0,
            pad=4
        ),
        yaxis=dict(
            autorange=True,
            showgrid=False,
            zeroline=False,
            showline=False,
            ticks='',
            showticklabels=False,
        )
    )


    under_color = "rgb(139,34,139)"
    over_color = "rgb(34,139,34)"

    fig = go.Figure(layout=layout)
    # Full line
    fig.add_scattergl(x=pd.to_datetime(df["time"], unit='s'), y=df["close"], line={'color': (over_color),'width':6})

    # Above threshhgold
    fig.add_scattergl(x=pd.to_datetime(df["time"], unit='s'), y=df["close"].where(df["close"] <= price), line={'color': (under_color),"width":6})
    #print(fig)

    py.image.save_as({'data': fig}, '/images/price.png',width=995, height=249)

