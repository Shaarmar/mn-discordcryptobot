import requests
import re
import simplejson as json

from ratelimit import limits
from datetime import datetime, timedelta,timezone
from fuzzywuzzy import fuzz
from fuzzywuzzy import process
import pandas as pd

from dateutil.parser import parse
import plotly.plotly as py
import plotly.offline as pyo
import plotly.graph_objs as go
import os
from dateutil import tz
import feedparser
import time
from html.parser import HTMLParser

from_zone = tz.gettz('UTC')
to_zone = tz.gettz('America/Chicago')

format = "%a %b %d %Y at %I:%M %p"
CRYPTO_COMPARE = os.environ['CRYPTO_COMPARE_API']
ONE_SECOND = 1

logo_url = "https://static.coinpaprika.com/coin/{0}/logo.png"
img_url = "https://static.coinpaprika.com/exchanges/{0}}/logo.png"
chart_url = "https://graphs.coinpaprika.com/currency/chart/{}/7d/svg"
link = "https://coinpaprika.com/coin/{}/"
exchange_link = "https://coinpaprika.com/exchanges/{}/"


@limits(calls=10, period=ONE_SECOND)
class Client(object):
    API_URL = 'https://api.coinpaprika.com'
    API_VERSION = 'v1'

    def __init__(self):
        self.session = self.init_session()

    def init_session(self):
        session = requests.session()
        session.headers.update({'Accept': 'application/json',
                                'Accept-Encoding': 'deflate, gzip'})
        return session

    def request_api_endpoint(self, endpoint, params={}, timeout=10):
        api_uri = self.API_URL + '/' + self.API_VERSION + '/' + endpoint

        response = self.session.get(url=api_uri, params=params, timeout=timeout)

        return response.json()
    def get_status(self, endpoint, params={}, timeout=10):
        api_uri = self.API_URL + '/' + self.API_VERSION + '/' + endpoint

        response = self.session.get(url=api_uri, params=params, timeout=timeout)
        return response.status_code


client = Client()


def GetID(symbol):
    data = json.loads(open('/json/symbol-coin.json').read())
    coin_id = data.get(symbol)

    if coin_id is not None:
        return coin_id
    else:
        print("Coin "+symbol+" not in symbol-coin.json")

def parseBody(s):
    h = HTMLParser()
    return h.unescape(s)

def news(i):
    news_url = "https://min-api.cryptocompare.com/data/v2/news/?lang=EN&sortOrder=latest&excludeCategories=Sponsored&api_key={0}".format(CRYPTO_COMPARE)
    data = requests.get(news_url)
    news = json.loads(data.content)

    title = news["Data"][i]["title"]
    url = news["Data"][i]["url"]
    published_on = news["Data"][i]["published_on"]
    body = news["Data"][i]["body"]

    api = 'http://api.linkpreview.net/?key=5c834c77a0949a576eafbce762079f7d1f77fca1c5cc2&q={0}'.format(url)
    response = requests.request("POST", api)
    api = response.json()

    img_url = api["image"]
    description = api["description"]
    body = parseBody(body)
    sep = 'The post '
    body = body.split(sep, 1)[0]

    published_on = time.strftime('%a %b %d %Y at %I:%M %p', time.localtime(published_on))
    return title, img_url, body, url,published_on


def global_info():

    global_response = client.request_api_endpoint("global")

    bitcoin_dominance_percentage = '{:,.2f}'.format(global_response["bitcoin_dominance_percentage"])+'%'

    cryptocurrencies_number= global_response["cryptocurrencies_number"]
    last_updated= datetime.fromtimestamp(global_response["last_updated"])
    last_updated = last_updated.strftime('%a %b %d %Y at %I:%M %p')
    market_cap_ath_date= '{:%m/%d/%Y}'.format(parse(global_response["market_cap_ath_date"]))
    market_cap_ath_value= '${:,.2f}'.format(global_response["market_cap_ath_value"])
    market_cap_change_24h= '{:,.2f}'.format(global_response["market_cap_change_24h"])+'%'
    market_cap_usd= '${:,.2f}'.format(global_response["market_cap_usd"])
    volume_24h_change_24h= '{:,.2f}'.format(global_response["volume_24h_change_24h"])+'%'
    volume_24h_usd= '${:,.2f}'.format(global_response["volume_24h_usd"])

    return cryptocurrencies_number,market_cap_ath_date,market_cap_ath_value,market_cap_change_24h,market_cap_usd,volume_24h_change_24h,volume_24h_usd,bitcoin_dominance_percentage,last_updated



def ticker_info(symbol):
    symbol = symbol.upper()
    coin_id = GetID(symbol)


    ticker_response = client.request_api_endpoint("tickers/{0}?quotes=USD,BTC".format(coin_id)) #by_id
    btc_price = '{:,.6f} SAT'.format(ticker_response['quotes']['BTC']['price'])
    cur_price =  '${:,.4f}'.format(ticker_response['quotes']['USD']['price'])
    oneHour ='{:,.2f}'.format(ticker_response['quotes']['USD']['percent_change_1h']) + '%'
    tfHour = '{:,.2f}'.format(ticker_response['quotes']['USD']['percent_change_24h']) + '%'
    svnDay = '{:,.2f}'.format(ticker_response['quotes']['USD']['percent_change_7d'])+ '%'
    name = ticker_response["name"]
    Vol = '${:,.2f}'.format(ticker_response['quotes']['USD']['volume_24h'])
    totalSup = '{:,}'.format(ticker_response["total_supply"])
    rank = ticker_response["rank"]
    circSup = '{:,}'.format(ticker_response["circulating_supply"])
    marketCap = '${:,.2f}'.format(ticker_response['quotes']['USD']['market_cap'])
    emoji = "▼" if oneHour.startswith("-") \
            else "▲"
    emoji1 = "▼" if tfHour.startswith("-") \
            else "▲"
    emoji2 = "▼" if svnDay.startswith("-") \
            else "▲"
    symbol = ticker_response['symbol']
    ath_date = ticker_response["quotes"]["USD"]["ath_date"]
    ath_price = '${:,.2f}'.format(ticker_response["quotes"]["USD"]["ath_price"])

    datetimeObject = parse(ticker_response["last_updated"])
    central = datetimeObject.astimezone(to_zone)

    last_updated = central.strftime(format)

    logo_url = "https://static.coinpaprika.com/coin/{0}/logo.png".format(coin_id)
    chart_url = "https://graphs.coinpaprika.com/currency/chart/{}/7d/svg".format(coin_id)
    link_url = "https://coinpaprika.com/coin/{}/".format(coin_id)

    return cur_price,oneHour,tfHour,svnDay,name,Vol,totalSup,rank,circSup,marketCap,emoji,emoji1,emoji2,symbol,ath_price,ath_date,last_updated,logo_url,chart_url,link_url,btc_price



def ticker_historical_info(symbol,date):

    try:
        coin_id = GetID(symbol.upper())
    except Exception as e:
        raise ValueError('Coin not found')

    try:
        ticker_historical_response = client.request_api_endpoint("tickers/{0}/historical?start={1}".format(coin_id,str(date)))  #?start=2019-01-01&end=2019-01-20 #start is all we need
    except Exception as e:
        raise ValueError('Invalid Date')

    market_cap = '${:,.2f}'.format(ticker_historical_response[0]["market_cap"])
    price = '${:,.2f}'.format(ticker_historical_response[0]["price"])
    timestamp = '{:%m/%d/%Y}'.format(parse(ticker_historical_response[0]["timestamp"]))
    volume_24h = '${:,.2f}'.format(ticker_historical_response[0]["volume_24h"])
    logo_url = "https://static.coinpaprika.com/coin/{0}/logo.png".format(coin_id)

    return market_cap,price,timestamp,volume_24h,logo_url



def ticker_historical_price(symbol):
    N = 7

    date_N_days_ago = datetime.now() - timedelta(days=N)
    date = date_N_days_ago.strftime('%Y-%m-%d')
    coin_id = GetID(symbol.upper())


    ticker_historical_response = client.request_api_endpoint("tickers/{0}/historical?start={1}".format(coin_id,str(date)))  #?start=2019-01-01&end=2019-01-20 #start is all we need
    price = ticker_historical_response[0]["price"]

    return price

def exchange_info1(exchange):
    with open('/json/exchange-ids.json') as f:
        data = json.load(f)

    exchange_id = process.extractOne(exchange, data)[2]

    try:
        id= data.get(exchange_id)
    except ValueError:
        raise ValueError('Exchange '+exchange+' is not in exchange-ids.json file')

    exchanges_by_id_response = client.request_api_endpoint("exchanges/{0}".format(id)) #by_id

    active = exchanges_by_id_response["active"]
    if active is True:
        active = "Active"
    else:
        active = "Inactive"

    adjusted_rank = exchanges_by_id_response["adjusted_rank"]

    api_status= exchanges_by_id_response["api_status"]
    if api_status is True:
        api_status = 'Active'
    else:
        api_status = "Inactive"

    currencies= '{:,}'.format(exchanges_by_id_response["currencies"])
    description= exchanges_by_id_response["description"]
    fiats= exchanges_by_id_response["fiats"]
    if len(fiats) is 0:
        fiats = None

    last_updated= '{:%m/%d/%Y}'.format(parse(exchanges_by_id_response["last_updated"]))
    website = exchanges_by_id_response["links"]["website"][0]
    markets= '{:,}'.format(exchanges_by_id_response["markets"])
    name= exchanges_by_id_response["name"]

    website_status= exchanges_by_id_response["website_status"]
    if website_status is True:
        website_status = "Active"
    else:
        website_status = "Inactive"

    img_url = "https://static.coinpaprika.com/exchanges/{0}/logo.png".format(id)


    return active,adjusted_rank,api_status,currencies,description,fiats,last_updated,website,markets,name,website_status,img_url


def price_converter_to_USD(amount,base):

    coin_id = GetID(base.upper())


    price_converter = client.request_api_endpoint("price-converter?base_currency_id={0}&quote_currency_id={1}&amount={2}".format(coin_id,"usd-us-dollars",amount)) #?base_currency_id=&quote_currency_id=
    return '${:,.2f}'.format(price_converter["price"])


def price_converter_to_crypto(amount,base):


    coin_id = GetID(base.upper())

    price_converter = client.request_api_endpoint("price-converter?base_currency_id={0}&quote_currency_id={1}&amount={2}".format("usd-us-dollars",coin_id,amount)) #?base_currency_id=&quote_currency_id=
    price = price_converter["price"]
    return '{:,.4f}'.format((price)) +" " + base.upper()



################## Functions not being used START ##################

def ohlc_info_today(symbol):
    coin_id = GetID(symbol.upper())
    ohlc_today_response = client.request_api_endpoint("coins/{0}/ohlcv/today".format(coin_id))
    return json.dumps(ohlc_today_response,indent = 2, sort_keys = True)


def ohlc_info_historical(symbol):
    coin_id = GetID(symbol.upper())

    # posibilty of changing N
    N = 365

    date_N_days_ago = datetime.utcnow() - timedelta(days=N)
    date_N_days_ago = date_N_days_ago.replace(microsecond=0)
    date_N_days_ago = date_N_days_ago.isoformat("T")+"Z"

    now = datetime.utcnow().replace(microsecond=0) # <-- get time in UTC
    now = now.isoformat("T") + "Z"


    coin_id = GetID(symbol)

    ohlc_historical_response = client.request_api_endpoint("coins/{0}/ohlcv/historical?start={1}&end={2}".format(coin_id,str(date_N_days_ago),str(now)))
    df = pd.DataFrame(ohlc_historical_response)
    for i in range(len(ohlc_historical_response)):
        ohlc_historical_response[i]["time_close"] = '{:%m/%d/%Y}'.format(parse(ohlc_historical_response[i]["time_close"]))
    trace = go.Candlestick(x=df['time_close'],
                open=df['open'],
                high=df['high'],
                low=df['low'],
                close=df['close'])
    data = [trace]
    pyo.plot(data, filename='simple_candlestick')


def coin_exchange_info(symbol):
    coin_id = GetID(symbol.upper())
    info = {}
    exchange_response = client.request_api_endpoint("coins/{0}/exchanges".format(coin_id))
    for i in range(len(exchange_response)):
        try:
            info[exchange_response[i]["name"]] = exchange_response[i]["fiats"][0]["symbol"]
        except Exception as e:
            pass
            info[exchange_response[i]["name"]] = exchange_response[i]["fiats"]

    return info

def ohlc_info_latests(symbol):
    coin_id = GetID(symbol.upper())
    ohlc_latest_response = client.request_api_endpoint("coins/{0}/ohlcv/latest".format(coin_id))
    print(type(ohlc_latest_response))
    df = pd.DataFrame(ohlc_latest_response)

def tag_info():
    dict1 = {}
    tags_response = client.request_api_endpoint("tags")
    for i in range(len(tags_response)):
        dict1[tags_response[i]["name"]] = (tags_response[i]["description"],tags_response[i]["coin_counter"])
    print(pd.DataFrame.from_dict(dict1,orient='index'))
    return json.dumps(tags_response,indent = 2, sort_keys = True)

def market_info(symbol):
    coin_id = GetID(symbol.upper())
    market_response = get_markets_response = client.request_api_endpoint("coins/{0}/markets".format(coin_id))
    for i in range(len(market_response)):
        base_currency_name = market_response[i]["base_currency_name"]
        exchange_name = market_response[i]["exchange_name"]
        last_updated = market_response[i]["last_updated"]
        market_url = market_response[i]["market_url"]
        pair = market_response[i]["pair"]
        quote_currency_name = market_response[i]["quote_currency_name"]
        quotes_price = market_response[i]["quotes"]["USD"]["price"]
        quotes_volume_24h = market_response[i]["quotes"]["USD"]["volume_24h"]
    return base_currency_name,exchange_name,last_updated,market_url,pair,quote_currency_name,quotes_price,quotes_volume_24h

def twitter_info(symbol):


    coin_id = GetID(symbol.upper())


    twitter_response = client.request_api_endpoint("coins/{0}/twitter".format(coin_id))
    tweet = twitter_response[0]['status_link']

    return tweet



def events_info(symbol):


    coin_id = GetID(symbol.upper())

    print(coin_id)
    events_response = client.request_api_endpoint("coins/{0}/events".format(coin_id))
    date = '{:%m/%d/%Y}'.format(parse(events_response[0]["date"]))
    date_to = events_response[0]["date_to"]
    description = events_response[0]["description"]
    is_conference = events_response[0]["is_conference"]
    name = events_response[0]["name"]

    return date,date_to,description,is_conference,name

def display_time(seconds, granularity=2):
    intervals = (
    ('weeks', 604800),  # 60 * 60 * 24 * 7
    ('days', 86400),    # 60 * 60 * 24
    ('hours', 3600),    # 60 * 60
    ('minutes', 60),
    ('seconds', 1),
    )

    result = []

    for name, count in intervals:
        value = seconds // count
        if value:
            seconds -= value * count
            if value == 1:
                name = name.rstrip('s')
            result.append("{} {}".format(value, name))
    return ', '.join(result[:granularity])

################## Functions not being used END ##################







