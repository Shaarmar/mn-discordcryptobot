import urllib.request, json
import bot_functions as bf


coins_response = bf.client.request_api_endpoint("coins")

def getJson(link):
    with urllib.request.urlopen(link) as url:
        data = json.loads(url.read().decode())
        return data

def get_tag_ids(dict_id):
    dict2 = {}
    for i in range(len(dict_id)):
        dict2[dict_id[i]['symbol'].upper()] = dict_id[i]['id']
    return dict2

def exchange_info():
    exchanges = {}
    exchangesList = []
    exchanges_response = bf.client.request_api_endpoint("exchanges")
    for i in range(len(exchanges_response)):
        exchangesList.append(exchanges_response[i]["name"])
        exchanges[exchanges_response[i]["name"]] = exchanges_response[i]["id"]
    return exchanges,exchangesList

def update_jsons():
    dict1,list1=exchange_info()
    with open('/json/exchange-ids.json','w') as outfile:
        y = json.dump(dict1,outfile,indent = 2, sort_keys = True)
    with open('/json/exchanges.json','w') as outfile:
        x = json.dump(list1,outfile,indent = 2, sort_keys = True)
    with open('/json/symbol-coin.json','w') as outfile:
        json.dump(get_tag_ids(coins_response),outfile,indent = 2, sort_keys = True)

update_jsons()
